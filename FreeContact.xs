/*  Copyright (C) 2013 by Laszlo Kajan, Technical University of Munich, Germany

    This program is free software; you can redistribute it and/or modify
    it under the same terms as Perl itself, either Perl version 5.10.1 or,
    at your option, any later version of Perl 5 you may have available.
*/
#include <freecontact.h>

#ifdef __cplusplus
extern "C" {
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"
}
#endif

using namespace freecontact;
typedef freecontact::predictor::cont_res_t	cont_res_t;
typedef freecontact::predictor::time_res_t	time_res_t;
typedef freecontact::predictor::freq_vec_t	freq_vec_t;

#define _XPUSHS_GET_PS(__ps) \
        XPUSHs(sv_2mortal(newSVpvn("clustpc", 7)));\
        XPUSHs(sv_2mortal(newSVnv((__ps).clustpc)));\
        XPUSHs(sv_2mortal(newSVpvn("density", 7)));\
        XPUSHs(sv_2mortal(newSVnv((__ps).density)));\
        XPUSHs(sv_2mortal(newSVpvn("gapth", 5)));\
        XPUSHs(sv_2mortal(newSVnv((__ps).gapth)));\
        XPUSHs(sv_2mortal(newSVpvn("mincontsep", 10)));\
        XPUSHs(sv_2mortal(newSVuv((__ps).mincontsep)));\
\
        XPUSHs(sv_2mortal(newSVpvn("pseudocnt", 9)));\
        XPUSHs(sv_2mortal(newSVnv((__ps).pseudocnt)));\
        XPUSHs(sv_2mortal(newSVpvn("pscnt_weight", 12)));\
        XPUSHs(sv_2mortal(newSVnv((__ps).pscnt_weight)));\
        XPUSHs(sv_2mortal(newSVpvn("estimate_ivcov", 14)));\
        XPUSHs(sv_2mortal(newSViv((__ps).estimate_ivcov)));\
        XPUSHs(sv_2mortal(newSVpvn("shrink_lambda", 13)));\
        XPUSHs(sv_2mortal(newSVnv((__ps).shrink_lambda)));\
\
        XPUSHs(sv_2mortal(newSVpvn("cov20", 5)));\
        XPUSHs(sv_2mortal(newSViv((__ps).cov20)));\
        XPUSHs(sv_2mortal(newSVpvn("apply_gapth", 11)));\
        XPUSHs(sv_2mortal(newSViv((__ps).apply_gapth)));\
        XPUSHs(sv_2mortal(newSVpvn("rho", 3)));\
        XPUSHs(sv_2mortal(newSVnv((__ps).rho)));

MODULE = FreeContact		PACKAGE = FreeContact		

INCLUDE_COMMAND: $^X -MExtUtils::XSpp::Cmd -e xspp -- --typemap=typemap.xsp FreeContact.xsp
