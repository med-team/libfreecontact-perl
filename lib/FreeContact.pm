#   FreeContact - program to predict protein residue contacts from a sufficiently large multiple alignment
#   Copyright (C) 2013 by Laszlo Kajan, Technical University of Munich, Germany
#  
#   This program is free software; you can redistribute it and/or modify
#   it under the same terms as Perl itself, either Perl version 5.10.1 or,
#   at your option, any later version of Perl 5 you may have available.
#  
package FreeContact;

use 5.010001;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);
use Carp;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use FreeContact ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
	get_ps_evfold
    get_ps_psicov
    get_ps_psicov_sd
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '0.08';

require XSLoader;
XSLoader::load('FreeContact', $VERSION);

# Preloaded methods go here.
sub                 FreeContact::Predictor::new
{
    my $class = shift(@_);
    if(scalar(@_)%2){ confess("Odd number of arguments"); }
    my %args = @_;
    return $class->_new(
        defined($args{dbg}) ? $args{dbg} : ()
    );
}

sub                 FreeContact::Predictor::get_seq_weights
{
    my $self = shift(@_);
    if(scalar(@_)%2){ confess("Odd number of arguments"); }
    my %args = (get_ps_evfold(), @_);
    my ($aliw, $wtot);
    $self->_get_seq_weights($aliw, $wtot,
        $args{ali}, $args{clustpc},
        defined($args{veczw}) ? $args{veczw} : 1, $args{num_threads} || 0
    );
    return ($aliw, $wtot);
}

sub                 FreeContact::Predictor::run
{
    my $self = shift(@_);
    if(scalar(@_)%2){ confess("Odd number of arguments"); }
    my %args = (get_ps_evfold(), @_);
    return $self->_run($args{ali}, $args{clustpc},
        $args{density}, $args{gapth}, $args{mincontsep},
        $args{pseudocnt}, $args{pscnt_weight}, $args{estimate_ivcov}, $args{shrink_lambda},
        $args{cov20}, $args{apply_gapth}, $args{rho},
        defined($args{veczw}) ? $args{veczw} : 1,
        $args{num_threads} || 0, defined($args{icme_timeout}) ? $args{icme_timeout} : 1800, $args{timing}
    );
}

sub                 FreeContact::Predictor::run_with_seq_weights
{
    my $self = shift(@_);
    if(scalar(@_)%2){ confess("Odd number of arguments"); }
    my %args = (get_ps_evfold(), @_);
    return $self->_run_with_seq_weights($args{ali}, $args{aliw}, $args{wtot},
        $args{density}, $args{gapth}, $args{mincontsep},
        $args{pseudocnt}, $args{pscnt_weight}, $args{estimate_ivcov}, $args{shrink_lambda},
        $args{cov20}, $args{apply_gapth}, $args{rho},
        $args{num_threads} || 0, defined($args{icme_timeout}) ? $args{icme_timeout} : 1800, $args{timing}
    );
}

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You'd better edit it!

=head1 NAME

FreeContact - fast protein contact predictor

=head1 SYNOPSIS

  use FreeContact;

  open(EXAMPLE, '<', '/usr/share/doc/libfreecontact-perl/examples/demo_1000.aln') || confess($!);
  my @aln = <EXAMPLE>; chomp(@aln); close(EXAMPLE);

  my $contacts = FreeContact::Predictor->new()->run(ali => \@aln);

  my $predictor = FreeContact::Predictor->new();
  my %parset = FreeContact::get_ps_evfold();
  my $contacts = $predictor->run(ali => \@aln, %parset, num_threads => 1);

  my $predictor = FreeContact::Predictor->new();
  my($aliw, $wtot) = $predictor->get_seq_weights(ali => \@aln, num_threads => 1);
  my $contacts = $predictor->run_with_seq_weights(ali => \@aln, aliw => $aliw, wtot => $wtot, num_threads => 1);

=head1 DESCRIPTION

FreeContact is a protein residue contact predictor optimized for speed.
Its input is a multiple sequence alignment. FreeContact can function as an
accelerated drop-in for the published contact predictors
EVfold-mfDCA of DS. Marks (2011) and
PSICOV of D. Jones (2011).
FreeContact is accelerated by a combination of vector instructions, multiple
threads, and faster implementation of key parts.
Depending on the alignment, 10-fold or higher speedups are possible.

A sufficiently large alignment is required for meaningful results.
As a minimum, an alignment with an effective (after-weighting) sequence count
bigger than the length of the query sequence should be used. Alignments with
tens of thousands of (effective) sequences are considered good input.

jackhmmer(1) from the hmmer package, or hhblits(1) from hhsuite
can be used to generate the alignments, for example.

=head1 FreeContact

=head2 EXPORT_OK

=over

=item get_ps_evfold()

Get parameters for EVfold-mfDCA operating mode.

=item get_ps_psicov()

Get parameters for PSICOV 'improved results' operating mode.

=item get_ps_psicov_sd()

Get parameters for PSICOV 'sensible default' operating mode. This is much faster than 'improved results' for a
slight loss of precision.

These get_ps_() functions return a hash of arguments (clustpc => I<num>,...,rho => I<num>) that can be used
with get_seq_weights(), run() or run_with_seq_weights(). The arguments correspond to the published parametrization of the
respective method.

=back

=head1 FreeContact::Predictor

=head2 Constructor

=over

=item new( dbg => bool )

Creates an "FreeContact::Predictor".

=back

=head2 Methods

=over

=item get_seq_weights()

Defaults for the arguments are obtained with get_ps_evfold().

=item run(ali => I<[]>, clustpc => dbl,
    density => dbl, gapth => dbl, mincontsep => uint,
    pseudocnt => dbl, pscnt_weight => dbl, estimate_ivcov => bool, shrink_lambda => dbl,
    cov20 => bool, apply_gapth => bool, rho => dbl,
    [veczw => bool], [num_threads => int], [icme_timeout => int], [timing => I<{}>])

Defaults for the arguments are obtained with get_ps_evfold().

=over

=item ali

Reference to array holding alignment rows as strings. The first
row must hold the query, with no gaps.

=item clustpc

BLOSUM-style clustering similarity threshold [0-1].

=item icme_timeout

Inverse covariance matrix estimation timeout in seconds. Default: 1800.

The estimation sometimes gets stuck. If the timeout is reached, the run()
method dies with "Caught FreeContact timeout exception: ...". You can catch
this exception and handle it as needed, e.g. by setting a higher B<rho> value.

=item num_threads

Number of OpenMP threads to use. If unset, all CPUs are used.

=item timing

If given, this hash reference is filled with data containing wall clock
timing results in seconds:

  {
    num_threads =>  NUM,
    seqw =>         NUM,
    pairfreq =>     NUM,
    shrink =>       NUM,
    inv =>          NUM,
    all =>          NUM
  }

=back

run() returns a hash reference of contact prediction results:

  {
    fro => [  # identifier of scoring scheme
      [
        I,    # 0-based index of amino acid i
        J,    # 0-based index of amino acid j
        SCORE # contact score
      ], ...
    ],
    MI => ...,
    l1norm => ...
  }

Use 'fro' scores with EVfold.

=back

=head1 AUTHOR

Laszlo Kajan, E<lt>lkajan@rostlab.orgE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Laszlo Kajan

This program is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.10.1 or,
at your option, any later version of Perl 5 you may have available.

=cut
# vim:et:ts=4:ai:textwidth=78:
