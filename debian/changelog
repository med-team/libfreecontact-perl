libfreecontact-perl (0.08-9) unstable; urgency=medium

  * Upstream download vanished
  * Standards-Version: 4.6.0 (routine-update)
  * Add missing build dependency on dh addon.

 -- Andreas Tille <tille@debian.org>  Wed, 19 Jan 2022 21:31:25 +0100

libfreecontact-perl (0.08-8) unstable; urgency=medium

  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Secure URI in copyright format (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Remove obsolete field Contact from debian/upstream/metadata (already present
    in machine-readable debian/copyright).
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 24 Nov 2020 15:39:22 +0100

libfreecontact-perl (0.08-7) unstable; urgency=medium

  * Drop get-orig-source target
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Mon, 24 Sep 2018 14:10:50 +0200

libfreecontact-perl (0.08-6) unstable; urgency=medium

  * Team upload

  [ Carnë Draug ]
  * Enable all hardening flags for build.
  * Enable autopkgtest

  [ Andreas Tille ]
  * debhelper 10
  * cme fix dpkg-control
  * Standards-Version: 4.1.0 (no changes needed)

 -- Carnë Draug <carandraug+dev@gmail.com>  Wed, 30 Aug 2017 17:11:31 +0100

libfreecontact-perl (0.08-5) unstable; urgency=medium

  * Fix __timing initialization (thanks a lot to Niko Tyni <ntyni@debian.org>
    for the patch)
    Closes: #812626
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Fri, 11 Mar 2016 11:07:04 +0100

libfreecontact-perl (0.08-4) unstable; urgency=medium

  * Do not hardcode libfreecontact0 dependency
    Closes: #796729

 -- Andreas Tille <tille@debian.org>  Mon, 24 Aug 2015 09:23:00 +0200

libfreecontact-perl (0.08-3) unstable; urgency=medium

  * cme fix dpkg-control
  * add myself to Uploaders
  * Priority: optional
  * Rebuild against gcc-5 transitioned libfreecontact-dev
    Closes: #795586
  * simplify debian/rules
  * DEP5 fix

 -- Andreas Tille <tille@debian.org>  Fri, 21 Aug 2015 11:42:20 +0200

libfreecontact-perl (0.08-2) unstable; urgency=low

  * Updated build dependency to new version of libfreecontact0-dev that fixes
    #724257.

 -- Laszlo Kajan <lkajan@rostlab.org>  Sat, 28 Sep 2013 22:51:20 +0200

libfreecontact-perl (0.08-1) unstable; urgency=low

  * Initial release (Closes: #723747).

 -- Laszlo Kajan <lkajan@rostlab.org>  Thu, 19 Sep 2013 15:46:28 +0200
