FreeContact is a protein residue contact predictor optimized for speed.
Its input is a multiple sequence alignment. FreeContact can function as an
accelerated drop-in for the published contact predictors
EVfold-mfDCA of DS. Marks (2011) and
PSICOV of D. Jones (2011).

FreeContact is accelerated by a combination of vector instructions, multiple
threads, and faster implementation of key parts.
Depending on the alignment, 8-fold or higher speedups are possible.

A sufficiently large alignment is required for meaningful results.
As a minimum, an alignment with an effective (after-weighting) sequence count
bigger than the length of the query sequence should be used. Alignments with
tens of thousands of (effective) sequences are considered good input.

jackhmmer(1) from the hmmer package, or hhblits(1) from hhsuite
can be used to generate the alignments, for example.
