use strict;
use warnings;

use Scalar::Util 'reftype';
use Test::More tests => 10;
BEGIN { use_ok('FreeContact') };

# /usr/share/doc/libextutils-xspp-perl/examples/Object-WithIntAndString/t/02test.t
sub check_obj {
    my $o = shift;
    isa_ok($o, 'FreeContact::Predictor');
}

{
    my $o = FreeContact::Predictor->new(dbg => 1); check_obj($o);
    eval {
        my $res = $o->run(undef);
    };
    like($@, qr/^Odd number of arguments/);
}

{
    open(EXAMPLE, '<', 'examples/demo_1000.aln') || confess($!);
    my @aln = <EXAMPLE>; chomp(@aln);
    close(EXAMPLE);

    my $num_threads = 1; # test will not work with 0
    my $timing = {};

    my $res = FreeContact::Predictor->new(dbg => 1)->run(ali => \@aln, num_threads => $num_threads, timing => $timing);

    my $evfold_24_42 = 0.0129471030086279; # 0-based indices
    my $prec_threshold = 3e-4;

    is(reftype($res), 'HASH');

    #use Data::Dumper; warn(Dumper($res->{fro}->[2741]));
    ok(abs($res->{fro}->[2741]->[2]-$evfold_24_42)/$evfold_24_42 < $prec_threshold, 'precision test'); # 25 K 43 N 0.230969 0.0129471
    ok($timing->{num_threads} == $num_threads, "timing results test");

    #use Data::Dumper; warn(Dumper($timing));

    my %test_parset = FreeContact::get_ps_psicov();
    eval {
        $res = FreeContact::Predictor->new(dbg => 1)->run(%test_parset, ali => \@aln, num_threads => $num_threads, icme_timeout => 2, timing => undef);
    };
    like($@, qr/^Caught FreeContact timeout exception:/);

    # aliw+wtot start test
    {
        my($aliw, $wtot) = FreeContact::Predictor->new(dbg => 1)->get_seq_weights(ali => \@aln, num_threads => $num_threads);
        #use Data::Dumper; warn(Dumper($aliw, \$wtot));
        is(@$aliw, 1000);
        ok(abs($wtot-620.777)/620.777 < 1e-6, 'wtot test');

        my $res = FreeContact::Predictor->new(dbg => 1)->run_with_seq_weights(ali => \@aln, aliw => $aliw, wtot => $wtot, num_threads => $num_threads);

        ok(abs($res->{fro}->[2741]->[2]-$evfold_24_42)/$evfold_24_42 < $prec_threshold, 'wtot precision test');
    }
}

# vim:et:ts=4:ai:
