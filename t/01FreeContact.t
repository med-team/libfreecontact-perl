# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl FreeContact.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 3;
BEGIN { use_ok('FreeContact') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

{
    can_ok('FreeContact', qw(get_ps_evfold get_ps_psicov get_ps_psicov_sd));
    can_ok('FreeContact::Predictor', qw(new _get_seq_weights get_seq_weights _run run));
}
# vim:et:ts=4:ai:
